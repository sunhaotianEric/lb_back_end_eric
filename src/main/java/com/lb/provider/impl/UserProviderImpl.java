package com.lb.provider.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.lb.config.shiro.MySessionManager;
import com.lb.model.user.User;
import com.lb.provider.UserProvider;
import com.lb.service.user.UserService;
import com.lb.util.RedisUtil;
import com.lb.util.UserUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.shiro.subject.support.DefaultSubjectContext.PRINCIPALS_SESSION_KEY;

@Component
@Service(interfaceClass = UserProvider.class, timeout = 2000, version = "1.0.0")
public class UserProviderImpl implements UserProvider {

    static AtomicInteger visitorid = new AtomicInteger(80000);

    @Autowired
    UserUtil userUtil;

    @Autowired
    UserService userService;

    @Autowired
    RedisUtil redisUtil;

    @Override
    public User checkLogin(String token) {
        User user = userUtil.getUserBytoken(token);
        if (user == null) {
            user = new User();
            user.setId(visitorid.addAndGet(1));
            user.setName("游客:" + visitorid.get());
            user.setAge(0);
            user.setSex(0);
            user.setGroupRole(4);  // 标识用户所在群组角色 1:群主 2:管理员 3:成员 4:游客
        } else {
            // 验证管理员
            String groupId = "1";
            if (checkManager(groupId, user.getId())) {
                user.setGroupRole(2); // 用户所在群组角色是管理员
            }else{
                user.setGroupRole(3); // 用户所在群组角色是(一般)成员
            }

        }
        return user;
    }

    @Override
    public User flushUser(Integer id) {
        User user = userService.getById(id);
        user.setPassword(null);
        return user;
    }

    // 验证管理员
    private boolean checkManager(String groupId, Integer userId) {
//    System.out.println(redisUtil.lGet("group:" + groupId + ":manager",0L,-1L));
        List managers = (List) redisUtil.lGet("group:" + groupId + ":manager", 0L, -1L);
        boolean isManager = false;
        for (Object manager : managers) {
            if (userId.equals(manager)) {
                isManager = true;
                break;
            }
        }
        return isManager;
    }

}

