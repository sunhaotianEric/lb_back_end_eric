package com.lb.provider;

import com.lb.model.group.Group;
import com.lb.model.vo.UserGroup;

import java.util.List;

public interface GroupProvider {


    String listGroupByUserId(Integer userid);

    String  updateMemberIdentity(Integer userId,Integer Groupid);
}
