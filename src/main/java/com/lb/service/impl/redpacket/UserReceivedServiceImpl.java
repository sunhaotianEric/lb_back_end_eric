package com.lb.service.impl.redpacket;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.redpacket.UserReceivedMapper;
import com.lb.model.redpacket.UserReceived;
import com.lb.service.redpacket.UserReceivedService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserReceivedServiceImpl extends ServiceImpl<UserReceivedMapper, UserReceived> implements UserReceivedService {

    @Override
    public List<UserReceived> list(Integer pId) {
        return baseMapper.list(pId);
    }
}
