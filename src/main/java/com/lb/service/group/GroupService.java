package com.lb.service.group;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.group.Group;
import com.lb.model.vo.UserGroup;

import java.util.List;

public interface GroupService extends IService<Group> {

    List<UserGroup> getListByUserId(Integer userId);

    UserGroup getOneByUserIdAndGroupid(Integer userId,Integer groupId);

    Integer changeIdentity(Integer groupId,Integer memberId,Integer role);

    Integer countManager(Integer goupId);

    void saveUserGroup(UserGroup userGroup);
}
