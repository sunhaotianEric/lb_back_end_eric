package com.lb.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.user.IpAddress;
import org.springframework.stereotype.Service;

@Service
public interface IpAddressService extends IService<IpAddress> {
}
