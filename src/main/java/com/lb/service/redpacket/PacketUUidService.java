package com.lb.service.redpacket;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.redpacket.PacketUUid;
import com.lb.util.WebsocketClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public interface PacketUUidService extends IService<PacketUUid> {
    Integer nextId();

    String UUid();

    List<Integer> UserId(Integer packetId);

    Integer noUserPacket(Integer packetId);

    Double sum(Integer pId);

    List<PacketUUid> selectOverdue();

    Integer counts(Integer packetId);

}
