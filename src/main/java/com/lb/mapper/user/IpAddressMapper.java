package com.lb.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.user.IpAddress;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IpAddressMapper extends BaseMapper<IpAddress> {
}
