package com.lb.mapper.FeaturesObj;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.FeaturesObj.Broadcast;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BroadcastMapper extends BaseMapper<Broadcast> {
    @Select("select id,content,time from lb_broadcast where way=0")
    List<Broadcast>  usingBro();
}
