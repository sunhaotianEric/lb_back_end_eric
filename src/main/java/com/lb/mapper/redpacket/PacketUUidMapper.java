package com.lb.mapper.redpacket;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.redpacket.PacketUUid;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PacketUUidMapper extends BaseMapper<PacketUUid> {
    @Select("select max(packet_id) from lb_packet_uuid")
    Integer nextId();
    @Select("select left(uuid(),8)")
    String UUid();

    @Select("select send_user_id from lb_packet_uuid where packet_id=#{packetId}")
    List<Integer> userId(Integer packetId);

    @Select("select count(*) from lb_packet_uuid where has_used=1 and packet_id=#{packetId}")
    Integer noUserPacket(Integer packetId);

    @Select("select count(*) from lb_packet_uuid where packet_id=#{packetId}")
    Integer counts(Integer packetId);

    @Select("select sum(packet_money) from lb_packet_uuid where packet_id=#{pId}")
    Double sum(Integer pId);

    @Select("SELECT packet_uuid,overdue_time,send_user_id from lb_packet_uuid where has_used=0")
    List<PacketUUid> selectOverdue();
}
