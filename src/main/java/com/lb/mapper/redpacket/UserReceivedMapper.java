package com.lb.mapper.redpacket;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.redpacket.UserReceived;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserReceivedMapper extends BaseMapper<UserReceived> {
    @Select("select uuid,id,user_id,content," +
            "money,get_time from lb_user_received where id=#{pId} order by money desc")
    List<UserReceived> list(Integer pId);
}
