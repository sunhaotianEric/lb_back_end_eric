package com.lb.controller.features;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lb.controller.criteria.BroadcastWsBody;
import com.lb.model.FeaturesObj.Announcement;
import com.lb.model.FeaturesObj.Broadcast;
import com.lb.service.FeaturesObj.AnnouncementService;
import com.lb.service.FeaturesObj.BroadcastService;
import com.lb.util.WebsocketClient;
import com.lb.util.common.response.ResponseObj;
import com.lb.util.wsRunnable.WsRunnable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@RestController
@RequestMapping("/api")
public class FeaturesObjController {
    @Autowired
    BroadcastService broadcastService;

    @Autowired
    AnnouncementService announcementService;

    @RequestMapping(value = "/get_broadcast", method = RequestMethod.GET)
    public ResponseObj getBroadcast() {

        return ResponseObj.createResponse(0, null, broadcastService.list());
    }


    @RequestMapping(value = "/get_broadcast_byid/{id}", method = RequestMethod.GET)
    public ResponseObj getBroadcast(@PathVariable("id") Integer id) {
        Broadcast broadcast = broadcastService.getOne(new QueryWrapper<Broadcast>().eq("id", id));
        return ResponseObj.createResponse(0, null, broadcast);
    }

    @RequestMapping(value = "/uesing_broadcast", method = RequestMethod.GET)
    public ResponseObj uesingBroadcast() {
        List<Broadcast> broadcast = broadcastService.list(new QueryWrapper<Broadcast>().eq("way", 0));
        return ResponseObj.createResponse(0, null, broadcast);
    }


    //显示数据库中公告的历史记录
    @RequestMapping(value = "/announcement", method = RequestMethod.GET)
    public ResponseObj announcement(@RequestParam Integer noticeId) {
        if (noticeId == 0) return ResponseObj.createResponse(0, "公告历史", announcementService.list());
        Announcement announcement = announcementService.getOne(new QueryWrapper<Announcement>().eq("notice_id", noticeId));
        return ResponseObj.createResponse(0, "id为" + noticeId + "的公告", announcement);
    }

    Timer nTimer = new Timer();
    WebsocketClient wclient = new WebsocketClient();
    @Value("${wsSocket.hostIP}")
    public String hostIp;

    //广播功能
    @RequestMapping(value = "/update_broadcast", method = RequestMethod.POST)
    public ResponseObj updateBroadcast() {
        List<Broadcast> list = broadcastService.usingBro();
        nTimer.cancel();
        nTimer.purge();
        nTimer=new Timer();
        for(Broadcast broadcast : list){
            nTimer.scheduleAtFixedRate(new TimerTask(){
                public void run() {
                        BroadcastWsBody bwb = new BroadcastWsBody(50, "爱你哟", 1
                                , broadcast.getContent());
                        wclient.connection(JSON.toJSONString(bwb), hostIp);
                }
            },0,broadcast.getTime()*1000);
        }
        return ResponseObj.createResponse(0, "a");
    }

}
