package com.lb.controller.features;

import com.lb.controller.features.entity.UserMessgeEntity;
import com.lb.util.RedisUtil;
import com.lb.util.common.response.ResponseObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/do")
public class DoUserIpController {
    @Autowired
    RedisUtil redisUtil;


    @PostMapping(value = "/putmsg")
    public void putMsg(@RequestBody UserMessgeEntity userMessgeEntity){
        System.err.println(userMessgeEntity);
        redisUtil.set("group:userIP:"+userMessgeEntity.getIp(),userMessgeEntity);
    }

}
