package com.lb.controller.file;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.lb.common.BasesettingProperties;
import com.lb.model.common.HttpResult;
import com.lb.service.file.FileService;
import com.lb.util.common.response.ResponseObj;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/pic")
public class FileController {

    @Value("${ftp.image_base_url}")
    private String IMAGE_BASE_URL;

    @Value("${ftp.basepath}")
    private String BASE_PATH;

    private static final long AVATAR_MAX_SIZE = 1 * 1024 * 1024;
    private static final List<String> AVATAR_TYPES = new ArrayList<String>();

    // 静态初始化器：用于初始化本类的静态成员
    static {
        AVATAR_TYPES.add("image/jpeg");
        AVATAR_TYPES.add("image/png");
        AVATAR_TYPES.add("image/gif");
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public HttpResult upload(HttpServletRequest request, MultipartFile file) {
        Long start = System.currentTimeMillis();
        HttpResult httpResult = new HttpResult();

        if (file.isEmpty() || file.getSize() > AVATAR_MAX_SIZE || !AVATAR_TYPES.contains(file.getContentType()))
            httpResult.setMsg("上传失败");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

        String now = dateFormat.format(new Date());

        String[] date = now.split("-");

        String year = date[0];
        String month = date[1];
        String day = date[2];
        //按照日期生成文件夹
        String path = "/" + year + "/" + month + "/" + day + "/";
        String savePath = BASE_PATH+ path;


        File newPath = new File(savePath);
        //生成文件名
        String fullName = file.getOriginalFilename();
        Integer index = fullName.lastIndexOf(".");
        String suffix = "";
        if (index != -1) {
            suffix = fullName.substring(index);
        }
        fullName = UUID.randomUUID().toString() + suffix;

        if (!newPath.exists()) {
            newPath.mkdirs();
        }

        File dest = new File(newPath, fullName);

        /*Thread t = */new Thread() {
            @Override
            public void run() {
                try {
                    file.transferTo(dest);
                } catch (IOException e) {
                    httpResult.setMsg("文件上传异常！" + e.getMessage());
                }
            }
        }.start();
        /*t.start();*/

        httpResult.setCode(1);
        Long end = System.currentTimeMillis();
        httpResult.setMsg(IMAGE_BASE_URL + path + fullName);
        System.err.println("耗时"+(end-start));

        return httpResult;
    }



    /*@Autowired
    FileService picService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST) // 等价于 @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public HttpResult upload(HttpServletRequest request, MultipartFile file) {
        try {
            HttpResult result = new HttpResult();
            // 判断文件大小
            if (!file.isEmpty() && file.getSize() < BasesettingProperties.uploadpicsize) {
                // 上传图片服务器并返回图片url
                String picurl = picService.upload(file);
                if (picurl != null) {
                    result.setCode(0);
                    result.setMsg(picurl);
                } else {
                    result.setCode(-1);
                    result.setMsg("上传失败");
                }
            } else {
                result.setCode(1);
                result.setMsg("文件大于" + (BasesettingProperties.uploadpicsize / 1024) + "KB");
            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }*/
}
