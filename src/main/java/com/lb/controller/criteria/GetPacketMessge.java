package com.lb.controller.criteria;

import lombok.Data;

@Data
public class GetPacketMessge {
    String uuid;

    double money;

    String sendPacketUser;

    String content;

    String profilePicture;

    String  getTime;

    double sendMoney;

    Integer redPacketCount;

    Integer redPacketSurplus;

    public GetPacketMessge() {
    }

    public GetPacketMessge(String uuid, double money, String sendPacketUser, String content,
                           String profilePicture, String getTime, double sendMoney, Integer redPacketCount, Integer redPacketSurplus) {
        this.uuid = uuid;
        this.money = money;
        this.sendPacketUser = sendPacketUser;
        this.content = content;
        this.profilePicture = profilePicture;
        this.getTime = getTime;
        this.sendMoney = sendMoney;
        this.redPacketCount = redPacketCount;
        this.redPacketSurplus = redPacketSurplus;
    }
}
