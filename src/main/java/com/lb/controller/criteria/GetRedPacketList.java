package com.lb.controller.criteria;

import lombok.Data;

@Data
public class GetRedPacketList {
    Double money;

    Integer userId;

    String getTime;

    String nickName;

    String url;


    public GetRedPacketList(Double money, Integer userId, String getTime, String nickName, String url) {
        this.money = money;
        this.userId = userId;
        this.getTime = getTime;
        this.nickName = nickName;
        this.url = url;
    }
}
