package com.lb.controller.criteria;

import lombok.Data;

@Data
public class LoginManageCriteria {

    private String account;

    private String passWord;

    private String groupId;

}
