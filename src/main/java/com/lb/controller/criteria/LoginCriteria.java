package com.lb.controller.criteria;

import lombok.Data;

@Data
public class LoginCriteria {

    private String account;

    private String passWord;

    private String name;

}
