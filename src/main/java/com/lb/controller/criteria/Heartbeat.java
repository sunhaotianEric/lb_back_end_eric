package com.lb.controller.criteria;

import lombok.Data;

@Data
public class Heartbeat {
    int command=13;

    byte data=-128;
}
