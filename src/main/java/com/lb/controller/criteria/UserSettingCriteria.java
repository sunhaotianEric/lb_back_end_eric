package com.lb.controller.criteria;

import lombok.Data;

@Data
public class UserSettingCriteria {

    String name;

    Integer sex;

    Integer age;

    String profilePicture;



}
