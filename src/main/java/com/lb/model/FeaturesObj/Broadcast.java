package com.lb.model.FeaturesObj;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("lb_broadcast")
public class Broadcast {
    Integer id;

    String content;
    //当前广播状态 0使用 1关闭
    Integer way;

    Long time;

}
