package com.lb.model.FeaturesObj;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("lb_announcement")
public class Announcement {
    Integer id;

    Integer noticeId;

    String content;

    Date createTime;
}
