package com.lb.model.redpacket;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("lb_user_received")
public class UserReceived {
    String uuid;

    Integer id;

    Integer userId;

    String content;

    double money;

    String getTime;

    public UserReceived(String uuid, Integer id, Integer userId, String content, double money, String getTime) {
        this.uuid = uuid;
        this.id = id;
        this.userId = userId;
        this.content = content;
        this.money = money;
        this.getTime = getTime;
    }
}
