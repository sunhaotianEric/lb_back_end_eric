package com.lb.model.redpacket;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("lb_packet_uuid")
public class PacketUUid {
    Integer sendUserId;

    Integer packetId;

    String packetUuid;

    int hasUsed;

    double packetMoney;

    String content;

    Date createTime;

    Date overdueTime;


    public PacketUUid(Integer sendUserId, Integer packetId, String packetUuid, int hasUsed, double packetMoney, String content, Date createTime, Date overdueTime) {
        this.sendUserId = sendUserId;
        this.packetId = packetId;
        this.packetUuid = packetUuid;
        this.hasUsed = hasUsed;
        this.packetMoney = packetMoney;
        this.content = content;
        this.createTime = createTime;
        this.overdueTime = overdueTime;
    }
}
