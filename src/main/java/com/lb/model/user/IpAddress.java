package com.lb.model.user;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("lb_ip_address")
public class IpAddress {

    Integer id;

    String ip;

    String city;

    String province;

    String country;

    String nickName;

    Date loginTime;

    public IpAddress() {
    }

    public IpAddress(Integer id, String ip, String city, String province, String country, String nickName, Date loginTime) {
        this.id = id;
        this.ip = ip;
        this.city = city;
        this.province = province;
        this.country = country;
        this.nickName = nickName;
        this.loginTime = loginTime;
    }
}
