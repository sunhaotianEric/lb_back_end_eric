package com.lb.model.user;

import lombok.Data;

@Data
public class ViewUser {

    Integer phone;

    String name;

    Integer sex;

    Integer age;

    String profilePicture;

    Double goldCoin;


    public ViewUser() {
    }

    public ViewUser(User user) {
        this.phone = user.getPhone();
        this.name = user.getName();
        this.sex = user.getSex();
        this.age = user.age;
        this.profilePicture = user.getProfilePicture();
        this.goldCoin = user.getGoldCoin();
    }
}
