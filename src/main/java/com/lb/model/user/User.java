package com.lb.model.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@TableName("lb_user")
public class User implements Serializable {
    @TableId(type = IdType.AUTO)
    Integer id;

    String accountName;

    String password;

    Integer phone;

    String name;

    Integer sex;

    Integer age;

    String profilePicture;

    Date createTime;

    Integer isForbidden;

    Date loginTime;

    Integer isCheckPhone;

    Double goldCoin;

    @TableField(exist=false)
    Integer groupRole; // 标识用户所在群组角色 1:群主 2:管理员 3:成员 4:游客
}
