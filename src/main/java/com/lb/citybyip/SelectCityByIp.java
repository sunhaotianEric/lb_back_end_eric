package com.lb.citybyip;

import com.lb.model.user.IpAddress;
import com.lb.service.user.IpAddressService;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SelectCityByIp {

    /*public static void main(String[] args) {
        SelectCityByIp selectCityByIp = new SelectCityByIp();
        try {
            selectCityByIp.testCity("59.34.5.250",1,"不该啊");
        }catch (Exception e){
            try{
                selectCityByIp.testCity("119.92.192.99",1,"不该啊");
            }catch (Exception a){
                a.printStackTrace();
            }

        }

    }*/


    Map<String ,File> map =new HashMap<>();
    Map<String,DatabaseReader> readerMap = new HashMap<>();
    public IpAddress testCity(String Ip,Integer id,String nickName,String url) throws Exception {
        if (map.get("database")==null){
            //GeoIP2-City 数据库文件，把数据库文件直接放d盘下(可以随意喜欢的位置)
            File database = new File(url);
            map.put("database",database);
        }

        // 创建 DatabaseReader对象
        DatabaseReader reader=null;
        if(readerMap.get("reader")==null){
            reader = new DatabaseReader.Builder(map.get("database")).build();
            readerMap.put("reader",reader);
        }
        reader=readerMap.get("reader");

        InetAddress ipAddress = InetAddress.getByName(Ip);

        CityResponse response = reader.city(ipAddress);

        Country country = response.getCountry();
      /* System.out.println("US："+country.getIsoCode());            // 'US'
        System.out.println("United States："+country.getName());               // 'United States'
        System.out.println("国家："+country.getNames().get("zh-CN")); // '美国'*/

        Subdivision subdivision = response.getMostSpecificSubdivision();
        /*System.out.println("Minnesota："+subdivision.getName());    // 'Minnesota' 省
        System.out.println("MN："+subdivision.getIsoCode()); // 'MN'
        System.out.println("US"+subdivision.getNames().get("zh-CN"));*/

        City city = response.getCity();
       /* System.out.println("Minneapolis："+city.getName()); // 'Minneapolis'  市
        System.out.println("US"+city.getNames().get("zh-CN"));
*/
        Postal postal = response.getPostal();
        /*System.out.println("55455："+postal.getCode()); // '55455'

        Location location = response.getLocation();
       /* System.out.println("44.9733："+location.getLatitude());  // 44.9733
        System.out.println("-93.2323："+location.getLongitude()); // -93.2323*/

        String cityNames=city.getNames().get("zh-CN")==null?city.getNames().get("en"):city.getNames().get("zh-CN");
        //System.err.println( cityNames);
        String minnesota=subdivision.getNames().get("zh-CN")==null?subdivision.getNames().get("en"):subdivision.getNames().get("zh-CN");
        //System.err.print( minnesota);
        IpAddress address= new IpAddress(id,Ip,cityNames,
                minnesota,country.getNames().get("zh-CN"),nickName, new Date());
        return address;
    }





}
