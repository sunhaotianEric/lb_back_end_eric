package com.lb.config.shiro;


import com.lb.model.user.User;
import com.lb.service.user.UserService;
import com.lb.util.Md5Util;
import com.lb.util.RedisUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.List;

public class MyShiroRealm extends AuthorizingRealm {

    @Autowired
    UserService userService;

    @Autowired
    RedisUtil redisUtil;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
   /*     User user = (User) SecurityUtils.getSubject().getPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //用户角色
        Role role = roleService.getRoleByUserId(user.getId());
        info.addRole(role.getName());

        //用户权限
        List<Permission> permissionList = permissionService.findAllByRoleId(role.getId());
        permissionList.forEach(permission -> info.addStringPermission(permission.getPermissionCode()));
*/
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        return info;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取用户的输入的账号.
        String username = (String) authenticationToken.getPrincipal();
        User user = userService.findByUsername(username.replaceAll(" ", "^"));

        if (user == null) throw new UnknownAccountException("用户不存在");
        if (1 == user.getIsForbidden()) {
            throw new LockedAccountException("用户被禁用"); // 帐号锁定
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user, //用户
                user.getPassword(), //密码
                getName()  //realm name
        );
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(Md5Util.SALF.getBytes()));
        // 当验证都通过后，把用户信息放在session里
        return authenticationInfo;
    }

    @Override
    protected void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            return;
        }
        User user = (User) principals.getPrimaryPrincipal();
        String key="shiro_redis_role:Userid_"+user.getId();
        redisUtil.del(key);
    }

}
