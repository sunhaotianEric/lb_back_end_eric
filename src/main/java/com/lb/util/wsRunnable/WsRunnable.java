package com.lb.util.wsRunnable;

import org.java_websocket.client.WebSocketClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
//@Component
public class WsRunnable {
    //@Resource
    WebSocketClient client;

    public void sendSome(String msg){
        if(client==null){
            System.err.println("没有成功注入");
        }
         client.send(msg);
    }

}
