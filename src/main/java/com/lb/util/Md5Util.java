package com.lb.util;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;

public class Md5Util {
    public static final String SALF = "lb_back_end";

    public static String getMd5(String password) {
        String md5 = new Md5Hash(password, ByteSource.Util.bytes(SALF.getBytes())).toString();
        return md5;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getMd5("123456"));

    }
}
