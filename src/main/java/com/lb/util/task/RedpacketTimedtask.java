package com.lb.util.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lb.model.redpacket.PacketUUid;
import com.lb.model.user.User;
import com.lb.service.redpacket.PacketUUidService;
import com.lb.service.user.UserService;
import com.lb.util.RedisUtil;
import lombok.Data;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.Map.Entry;

@Component
@Configurable
public class RedpacketTimedtask {

    /* @Autowired
     RedpacketService rpService;
 */
    @Autowired
    RedisUtil redis;

    @Autowired
    UserService userService;

    @Autowired
    PacketUUidService packetUUidService;


    //--------------------------------
    //---------------------------------
    @Scheduled(cron = "0/30 * * * * ?")
    @Transactional
    public void task() {
//         每十秒和数据库通信一次太消耗性能，解决思路：用hash缓存将list的数据缓存到hash中
        //再判断红包uuid数据库里数据是否有变动，有的话，就重新更新hash表，没有的话。。。
       /* Set<String> redpacketSet = redis.keys("redpacket:send*");
        System.out.println(redpacketSet);*/

        List<PacketUUid> packetUUid =packetUUidService.list(new QueryWrapper<PacketUUid>().eq("has_used",0));
        if(packetUUid.isEmpty()){
            System.err.println("清理紅包進程正在繼續");
            return;
        }

        for (PacketUUid packet : packetUUid){
            long overdue=packet.getOverdueTime().getTime();
            long now =new Date().getTime();

            if ( now==overdue||now>overdue){
                System.err.println(packet+"過期了");
                Integer userId=packet.getSendUserId();
                User user=
                        userService.getOne(new QueryWrapper<User>().eq("id",userId));
                if(user==null) {
                    packetUUidService.remove(new QueryWrapper<PacketUUid>().eq("send_user_id",userId));
                    System.err.println("发送红包对应用户已经不存在，开始移除用户红包操作");
                    continue;
                };
                packet.setHasUsed(1);
                packetUUidService.
                        update(packet,new QueryWrapper<PacketUUid>().eq("packet_uuid",packet.getPacketUuid()));

                double total=user.getGoldCoin()+packet.getPacketMoney();

                user.setGoldCoin(total);
                userService.update(user,new QueryWrapper<User>().eq("id",userId));

            }
        }

    }

    /*  // 处理过期红包
    private boolean outofdateManage(String key, Map<String, Object> rp){
        // 返还金额
        rpService.changeGoldcoin((Integer) rp.get("user_id"), (Float) rp.get("remaining_money"));
        // 标记过期
        rpService.markOutofdate(Integer.parseInt(key));
        // 清理redis缓存
        redis.del(key);
        return true;
    }*/
}
