/*
Navicat MySQL Data Transfer

Source Server         : 23.225.140.154
Source Server Version : 50726
Source Host           : 23.225.140.154:3306
Source Database       : lb-im

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-30 22:11:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lb_announcement
-- ----------------------------
DROP TABLE IF EXISTS `lb_announcement`;
CREATE TABLE `lb_announcement` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '公告id',
  `notice_id` int(11) NOT NULL COMMENT '公告id',
  `content` varchar(255) DEFAULT NULL COMMENT '公告内容',
  `create_time` date DEFAULT NULL COMMENT '公告创建时间',
  PRIMARY KEY (`id`,`notice_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_announcement
-- ----------------------------
INSERT INTO `lb_announcement` VALUES ('00000000001', '1', '亲爱的游客朋友们，注册登录聊天室账号即可参与抢红包，红包满100可提现，详情点击【客服】咨询，每日上万红包等你来抢，一边看片一边抢红包，还能和美女互动，等什么赶快行动起来吧~', '2019-10-14');

-- ----------------------------
-- Table structure for lb_app_operating
-- ----------------------------
DROP TABLE IF EXISTS `lb_app_operating`;
CREATE TABLE `lb_app_operating` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '应用id',
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `open_the_way` varchar(255) DEFAULT NULL COMMENT '打开方式',
  `statu` int(2) DEFAULT NULL COMMENT '状态 0为开启1为关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_app_operating
-- ----------------------------
INSERT INTO `lb_app_operating` VALUES ('1', '绝地求生', '不知道', '0');

-- ----------------------------
-- Table structure for lb_basesetting
-- ----------------------------
DROP TABLE IF EXISTS `lb_basesetting`;
CREATE TABLE `lb_basesetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bsname` varchar(32) DEFAULT NULL COMMENT '参数名称',
  `bsvalue` varchar(32) DEFAULT NULL COMMENT '参数值',
  `remark` varchar(32) DEFAULT NULL COMMENT '备注',
  `bsexplain` varchar(255) DEFAULT NULL COMMENT '参数说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='基本参数设置';

-- ----------------------------
-- Records of lb_basesetting
-- ----------------------------
INSERT INTO `lb_basesetting` VALUES ('1', 'uploadpicsize', '1024', '单位(kB)', '上传图片大小限制');
INSERT INTO `lb_basesetting` VALUES ('2', 'expiretime', '12', '单位：小时', '红包过期时间(默认24小时)');
INSERT INTO `lb_basesetting` VALUES ('3', 'minmoney', '0.1', '金币单位：元', '拼手气红包最低金额(建议从\"0.01/0.1/1\"三选一)');

-- ----------------------------
-- Table structure for lb_broadcast
-- ----------------------------
DROP TABLE IF EXISTS `lb_broadcast`;
CREATE TABLE `lb_broadcast` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT '广播id',
  `content` varchar(255) DEFAULT NULL COMMENT '广播内容',
  `way` int(6) DEFAULT NULL COMMENT '状态：0使用 1关闭',
  `time` bigint(6) DEFAULT NULL COMMENT '广播间隔',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_broadcast
-- ----------------------------
INSERT INTO `lb_broadcast` VALUES ('1', '定时下红包雨', '1', '6000000000');
INSERT INTO `lb_broadcast` VALUES ('2', '各位亲们，欢迎来到pili pili亚洲最新成人AV电影聊天室，注册账号可参与抢每天不定时的红包雨，红包满100可兑换金币，详情请加聊天室客服QQ', '0', '165465');
INSERT INTO `lb_broadcast` VALUES ('3', '紧急通知： 本站计划由9月16日至10月7日进行服务器临时维护，维护期间将无法登录APP请您谅解！官方QQ群 471883385 聊天室正常运营。', '0', '16546');

-- ----------------------------
-- Table structure for lb_group
-- ----------------------------
DROP TABLE IF EXISTS `lb_group`;
CREATE TABLE `lb_group` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '房间名',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='群组';

-- ----------------------------
-- Records of lb_group
-- ----------------------------
INSERT INTO `lb_group` VALUES ('1', '测试房', 'http:22', '2019-08-22 19:58:14', '2019-08-22 19:58:16');
INSERT INTO `lb_group` VALUES ('2', '公共房', 'http:221', '2019-08-29 11:04:00', '2019-08-29 11:04:03');

-- ----------------------------
-- Table structure for lb_ip_address
-- ----------------------------
DROP TABLE IF EXISTS `lb_ip_address`;
CREATE TABLE `lb_ip_address` (
  `id` int(8) DEFAULT NULL COMMENT '用户id',
  `ip` varchar(50) DEFAULT NULL COMMENT '用户访问IP',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `login_time` datetime DEFAULT NULL COMMENT '登陆时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_ip_address
-- ----------------------------
INSERT INTO `lb_ip_address` VALUES ('1', '112.224.17.210', '淄博', '山东省', '中国', 'root', '2019-10-18 19:42:25');
INSERT INTO `lb_ip_address` VALUES ('1', '119.92.192.99', 'Makati City', 'Metro Manila', '菲律宾', 'root', '2019-10-18 19:48:15');
INSERT INTO `lb_ip_address` VALUES ('41', '59.33.92.255', '广州', '广东', '中国', 'ww8010', '2019-10-22 17:41:41');
INSERT INTO `lb_ip_address` VALUES ('69', '49.7.3.237', '开封市', '河南', '中国', 'zyg57982588', '2019-10-24 15:13:49');
INSERT INTO `lb_ip_address` VALUES ('77', '223.104.65.164', '揭阳市', '广东', '中国', '2129244645', '2019-10-24 15:15:48');
INSERT INTO `lb_ip_address` VALUES ('84', '183.13.122.255', '深圳市', '广东', '中国', '18581396797', '2019-10-24 15:18:07');
INSERT INTO `lb_ip_address` VALUES ('50', '218.26.54.214', '太原', '山西', '中国', '111222', '2019-10-24 15:20:29');
INSERT INTO `lb_ip_address` VALUES ('47', '182.131.73.157', null, '四川省', '中国', '赌神', '2019-10-24 15:26:52');
INSERT INTO `lb_ip_address` VALUES ('58', '119.92.192.100', 'Makati City', 'Metro Manila', '菲律宾', '小露', '2019-10-24 15:27:21');
INSERT INTO `lb_ip_address` VALUES ('74', '119.92.192.103', 'Makati City', 'Metro Manila', '菲律宾', '莉莉安', '2019-10-24 15:27:26');
INSERT INTO `lb_ip_address` VALUES ('64', '119.92.192.103', 'Makati City', 'Metro Manila', '菲律宾', '成都优质单男李老八', '2019-10-24 16:09:24');
INSERT INTO `lb_ip_address` VALUES ('53', '117.136.46.202', '苏州', '江苏省', '中国', '蝴蝶结', '2019-10-24 17:09:56');
INSERT INTO `lb_ip_address` VALUES ('51', '182.46.130.153', null, '山东省', '中国', '丰辉12171', '2019-10-24 17:11:16');
INSERT INTO `lb_ip_address` VALUES ('120', '119.92.192.100', 'Makati City', 'Metro Manila', '菲律宾', '计划员战狼神', '2019-10-30 22:06:45');

-- ----------------------------
-- Table structure for lb_packet_uuid
-- ----------------------------
DROP TABLE IF EXISTS `lb_packet_uuid`;
CREATE TABLE `lb_packet_uuid` (
  `packet_id` int(10) NOT NULL COMMENT '红包id',
  `packet_uuid` varchar(20) NOT NULL COMMENT '红包的uuid用于识别红包',
  `has_used` int(2) NOT NULL DEFAULT '0' COMMENT '0为未使用1为使用',
  `packet_money` double(10,2) DEFAULT NULL COMMENT '随机生成的红包红包金额',
  `send_user_id` int(10) DEFAULT NULL COMMENT '发送红包的用户id',
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `overdue_time` datetime DEFAULT NULL COMMENT '红包过期时间',
  `content` varchar(50) DEFAULT NULL COMMENT '红包文本',
  PRIMARY KEY (`packet_uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_packet_uuid
-- ----------------------------
INSERT INTO `lb_packet_uuid` VALUES ('119', '01750bbf', '1', '6.66', '71', '2019-10-30 12:51:59', '2019-10-31 12:51:59', '恭喜发财');
INSERT INTO `lb_packet_uuid` VALUES ('119', '0175ab3d', '1', '7.00', '71', '2019-10-30 12:51:59', '2019-10-31 12:51:59', '恭喜发财');
INSERT INTO `lb_packet_uuid` VALUES ('1', '初始记录误删', '1', '10.00', '1', '2019-10-08 00:00:00', '2019-10-10 00:00:00', 'string');

-- ----------------------------
-- Table structure for lb_role
-- ----------------------------
DROP TABLE IF EXISTS `lb_role`;
CREATE TABLE `lb_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `abbr` varchar(50) DEFAULT NULL COMMENT '英文简称',
  `is_forbidden` int(5) DEFAULT NULL COMMENT '是否禁用 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色';

-- ----------------------------
-- Records of lb_role
-- ----------------------------
INSERT INTO `lb_role` VALUES ('1', '注册用户', 'register_user', '0');
INSERT INTO `lb_role` VALUES ('2', '会员1', 'vip1', '0');
INSERT INTO `lb_role` VALUES ('3', '会员2', 'vip2', '0');

-- ----------------------------
-- Table structure for lb_topupcash_log
-- ----------------------------
DROP TABLE IF EXISTS `lb_topupcash_log`;
CREATE TABLE `lb_topupcash_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '充值提现id',
  `userid` bigint(20) NOT NULL COMMENT '用户id',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(64) DEFAULT NULL COMMENT '用户昵称',
  `type` int(11) DEFAULT NULL COMMENT '类型 0:保留，1:充值，2:提现',
  `money` double DEFAULT NULL COMMENT '金额',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `operater` bigint(20) DEFAULT NULL COMMENT '操作人id',
  `operatername` varchar(50) DEFAULT NULL COMMENT '操作人姓名',
  `operatetime` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='充值提现记录';

-- ----------------------------
-- Records of lb_topupcash_log
-- ----------------------------
INSERT INTO `lb_topupcash_log` VALUES ('1', '3', 'alex', 'alex', '1', '100', '100', '1', 'admin', '2019-10-10 13:16:11');
INSERT INTO `lb_topupcash_log` VALUES ('2', '15', 'zihua', '聊天室管理员：小可', '1', '9999999', '发红包', '1', 'admin', '2019-10-14 23:56:04');
INSERT INTO `lb_topupcash_log` VALUES ('3', '59', '聊天室管理员：小可', '聊天室管理员：小可', '1', '99999', '发红包', '1', 'admin', '2019-10-24 12:50:02');
INSERT INTO `lb_topupcash_log` VALUES ('4', '71', '管理员小可', '管理员小可', '1', '99999', '发红包', '1', 'admin', '2019-10-24 12:59:06');
INSERT INTO `lb_topupcash_log` VALUES ('5', '69', 'zyg57982588', 'zyg57982588', '1', '383.98', '旧聊天室红包转移', '1', 'admin', '2019-10-24 13:01:19');

-- ----------------------------
-- Table structure for lb_user
-- ----------------------------
DROP TABLE IF EXISTS `lb_user`;
CREATE TABLE `lb_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `phone` int(20) DEFAULT NULL COMMENT '手机号码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `sex` int(5) DEFAULT NULL COMMENT '0未设置 1男 2女',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `profile_picture` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_forbidden` int(5) DEFAULT NULL COMMENT '是否禁用 0否 1是',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `is_check_phone` int(5) DEFAULT NULL COMMENT '手机是否验证 0否 1是',
  `gold_coin` double(11,2) DEFAULT '0.00' COMMENT '金币',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='聊天用户';

-- ----------------------------
-- Records of lb_user
-- ----------------------------
INSERT INTO `lb_user` VALUES ('2', 'string', '4fc3701e00189d40132e5b83ef2d3722', null, 'string', '0', '0', null, '2019-10-08 11:41:46', '0', null, null, '10.32');
INSERT INTO `lb_user` VALUES ('3', 'alex', '4fc3701e00189d40132e5b83ef2d3722', null, 'alex', '0', '0', '', '2019-10-09 14:04:00', '0', null, null, '201.25');
INSERT INTO `lb_user` VALUES ('4', 'zhang', '4fc3701e00189d40132e5b83ef2d3722', null, 'zhang11', '0', '0', 'http://43.241.252.72:8083/2019/10/17/1571302739736273.jpeg', '2019-10-09 16:42:33', '0', null, null, '14.64');
INSERT INTO `lb_user` VALUES ('5', 'qw4530847', '4fc3701e00189d40132e5b83ef2d3722', null, '大海', '1', '18', 'http://43.241.252.72:8083/2019/10/17/1571292347480117.jpeg', '2019-10-10 14:16:52', '0', null, null, '600.00');
INSERT INTO `lb_user` VALUES ('6', 'eric', '4fc3701e00189d40132e5b83ef2d3722', null, 'eric', '0', '0', null, '2019-10-10 15:39:09', '0', null, null, '170.97');
INSERT INTO `lb_user` VALUES ('9', 'eric2', '4fc3701e00189d40132e5b83ef2d3722', null, 'eric2', '0', '0', 'http://43.241.252.72:8083/2019/10/24/1571906826646219.jpeg', '2019-10-11 18:15:10', '0', null, null, '113.83');

-- ----------------------------
-- Table structure for lb_user_group
-- ----------------------------
DROP TABLE IF EXISTS `lb_user_group`;
CREATE TABLE `lb_user_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL COMMENT '用户id',
  `group_id` int(200) DEFAULT NULL COMMENT '群组id',
  `nick` varchar(255) DEFAULT NULL COMMENT '在群昵称',
  `role` int(10) DEFAULT NULL COMMENT '在群角色 1群主 2管理员 3成员 4游客',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_user_group
-- ----------------------------
INSERT INTO `lb_user_group` VALUES ('15', '3', '1', null, '2');
INSERT INTO `lb_user_group` VALUES ('18', '6', '1', null, '3');
INSERT INTO `lb_user_group` VALUES ('19', '7', '1', null, '3');
INSERT INTO `lb_user_group` VALUES ('20', '8', '1', null, '3');
INSERT INTO `lb_user_group` VALUES ('21', '9', '1', null, '3');
INSERT INTO `lb_user_group` VALUES ('93', '82', '1', '', '2');
INSERT INTO `lb_user_group` VALUES ('97', '86', '1', '', '2');
INSERT INTO `lb_user_group` VALUES ('98', '87', '1', null, '3');


-- ----------------------------
-- Table structure for lb_user_received
-- ----------------------------
DROP TABLE IF EXISTS `lb_user_received`;
CREATE TABLE `lb_user_received` (
  `uuid` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `money` double(20,2) NOT NULL,
  `content` varchar(20) DEFAULT NULL,
  `get_time` varchar(50) DEFAULT NULL COMMENT '抢到红包的时间',
  PRIMARY KEY (`id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_user_received
-- ----------------------------
INSERT INTO `lb_user_received` VALUES ('01761cf5', '119', '118', '0.22', '恭喜发财', '2019-10-30 12:54:00');
INSERT INTO `lb_user_received` VALUES ('01750bbf', '119', '128', '6.66', '恭喜发财', '2019-10-30 12:52:44');
INSERT INTO `lb_user_received` VALUES ('0176fe04', '119', '48', '5.32', '恭喜发财', '2019-10-30 13:20:13');
INSERT INTO `lb_user_received` VALUES ('01768508', '119', '6', '5.28', '恭喜发财', '2019-10-30 13:19:37');
INSERT INTO `lb_user_received` VALUES ('0175ab3d', '119', '68', '7.00', '恭喜发财', '2019-10-30 12:53:23');

-- ----------------------------
-- Table structure for lb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `lb_user_role`;
CREATE TABLE `lb_user_role` (
  `role_id` int(20) NOT NULL,
  `user_id` int(200) DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of lb_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `param_key` (`param_key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '立博集团', '0', '0');
INSERT INTO `sys_dept` VALUES ('2', '1', '大陆分公司', '1', '0');
INSERT INTO `sys_dept` VALUES ('3', '1', '菲律宾分公司', '2', '0');
INSERT INTO `sys_dept` VALUES ('4', '3', '客服部', '0', '0');
INSERT INTO `sys_dept` VALUES ('5', '3', '技术部', '1', '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '字典名称',
  `type` varchar(100) NOT NULL COMMENT '字典类型',
  `code` varchar(100) NOT NULL COMMENT '字典码',
  `value` varchar(1000) NOT NULL COMMENT '字典值',
  `order_num` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `type` (`type`,`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数据字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '性别', 'sex', '0', '女', '0', null, '0');
INSERT INTO `sys_dict` VALUES ('2', '性别', 'sex', '1', '男', '1', null, '0');
INSERT INTO `sys_dict` VALUES ('3', '性别', 'sex', '2', '未知', '3', null, '0');
INSERT INTO `sys_dict` VALUES ('4', '聊天室角色', 'grouprole', '0', '未知', '0', '聊天室角色grouprole', '-1');
INSERT INTO `sys_dict` VALUES ('5', '聊天室角色', 'grouprole', '1', '群主', '1', '聊天室角色grouprole', '0');
INSERT INTO `sys_dict` VALUES ('6', '聊天室角色', 'grouprole', '2', '管理员', '2', '聊天室角色grouprole', '0');
INSERT INTO `sys_dict` VALUES ('7', '聊天室角色', 'grouprole', '3', '成员', '3', '聊天室角色grouprole', '0');
INSERT INTO `sys_dict` VALUES ('8', '聊天室角色', 'grouprole', '4', '游客', '4', '聊天室角色grouprole', '0');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '保存用户', 'com.lb.admin.modules.sys.controller.SysUserController.save()', '{\"userId\":2,\"username\":\"test\",\"password\":\"3c98b62cde14c68f3e8a9a3bf7cc61e17662221b10ba238f73eb275903681727\",\"salt\":\"5HOkdfBkfUkKvaf69PRz\",\"email\":\"123456@gmail.com\",\"mobile\":\"13812345678\",\"status\":1,\"roleIdList\":[],\"createTime\":\"Oct 5, 2019 3:26:51 PM\",\"deptId\":1,\"deptName\":\"立博集团\"}', '72', '0:0:0:0:0:0:0:1', '2019-10-05 15:26:51');
INSERT INTO `sys_log` VALUES ('2', 'admin', '修改用户', 'com.lb.admin.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":\"lbadmin\",\"salt\":\"5HOkdfBkfUkKvaf69PRz\",\"email\":\"123456@gmail.com\",\"mobile\":\"13812345678\",\"status\":1,\"roleIdList\":[],\"createTime\":\"Oct 5, 2019 3:26:51 PM\",\"deptId\":4,\"deptName\":\"客服部\"}', '189', '0:0:0:0:0:0:0:1', '2019-10-07 17:21:11');
INSERT INTO `sys_log` VALUES ('3', 'admin', '保存用户', 'com.lb.admin.modules.sys.controller.SysUserController.save()', '{\"userId\":3,\"username\":\"qw4530847\",\"password\":\"f1a64d35b4136275088eddead11f78a76d4db9602cd6fd3bcec8153442fa76cf\",\"salt\":\"wkZy1LoL4QOG8lYLvbY5\",\"email\":\"2176169440@qq.com\",\"mobile\":\"13205556363\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"Oct 14, 2019 8:33:17 AM\",\"deptId\":4,\"deptName\":\"客服部\"}', '64', '119.92.192.99', '2019-10-14 20:33:18');
INSERT INTO `sys_log` VALUES ('4', 'admin', '保存用户', 'com.lb.admin.modules.sys.controller.SysUserController.save()', '{\"userId\":4,\"username\":\"pite\",\"password\":\"87bbf0c9bbc4c04a6d96155bbe05a908235394f3b021868ba024cd1003418270\",\"salt\":\"T9p4ywCExKOojhmJFhJ1\",\"email\":\"1501895123@qq.com\",\"status\":1,\"roleIdList\":[2],\"createTime\":\"Oct 24, 2019 1:48:44 AM\",\"deptId\":1,\"deptName\":\"立博集团\"}', '31', '119.92.192.100', '2019-10-24 13:48:45');
INSERT INTO `sys_log` VALUES ('5', 'admin', '修改用户', 'com.lb.admin.modules.sys.controller.SysUserController.update()', '{\"userId\":4,\"username\":\"pite\",\"salt\":\"T9p4ywCExKOojhmJFhJ1\",\"email\":\"1501895123@qq.com\",\"status\":1,\"roleIdList\":[2,1],\"createTime\":\"Oct 24, 2019 1:48:45 AM\",\"deptId\":1,\"deptName\":\"立博集团\"}', '40', '119.92.192.100', '2019-10-24 13:50:02');
INSERT INTO `sys_log` VALUES ('6', 'admin', '删除用户', 'com.lb.admin.modules.sys.controller.SysUserController.delete()', '[4]', '16', '119.92.192.100', '2019-10-24 13:50:33');
INSERT INTO `sys_log` VALUES ('7', 'admin', '保存用户', 'com.lb.admin.modules.sys.controller.SysUserController.save()', '{\"userId\":5,\"username\":\"yumi\",\"password\":\"e3c7b457848ac629e0990eaeeb4f982382a31c1f165fa1ddc295a02867b81658\",\"salt\":\"l0oot3GqKu9u4dVggm6H\",\"email\":\"1501895123@qq.com\",\"mobile\":\"\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"Oct 24, 2019 2:05:25 AM\",\"deptId\":1,\"deptName\":\"立博集团\"}', '16', '119.92.192.100', '2019-10-24 14:05:26');
INSERT INTO `sys_log` VALUES ('8', 'admin', '删除用户', 'com.lb.admin.modules.sys.controller.SysUserController.delete()', '[5]', '27', '119.92.192.100', '2019-10-24 14:07:28');
INSERT INTO `sys_log` VALUES ('9', 'admin', '保存用户', 'com.lb.admin.modules.sys.controller.SysUserController.save()', '{\"userId\":6,\"username\":\"pite\",\"password\":\"cbf4d76f43a72738cb7e76f303dd7e9abf1cc4851cb6325ec25e8fa65b396c91\",\"salt\":\"QeSYM9npIXkotr3EZ79Q\",\"email\":\"1501895123@qq.com\",\"status\":1,\"roleIdList\":[1,2],\"createTime\":\"Oct 24, 2019 2:09:06 AM\",\"deptId\":1,\"deptName\":\"立博集团\"}', '42', '119.92.192.100', '2019-10-24 14:09:06');
INSERT INTO `sys_log` VALUES ('10', 'admin', '删除用户', 'com.lb.admin.modules.sys.controller.SysUserController.delete()', '[6]', '18', '119.92.192.100', '2019-10-24 14:12:11');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` VALUES ('2', '0', '后台管理', null, null, '0', 'fa fa-th-list', '0');
INSERT INTO `sys_menu` VALUES ('11', '1', '管理员管理', 'modules/sys/user.html', null, '1', 'fa fa-user', '1');
INSERT INTO `sys_menu` VALUES ('12', '1', '角色管理', 'modules/sys/role.html', null, '1', 'fa fa-user-secret', '2');
INSERT INTO `sys_menu` VALUES ('13', '1', '菜单管理', 'modules/sys/menu.html', null, '1', 'fa fa-th-list', '3');
INSERT INTO `sys_menu` VALUES ('14', '1', 'SQL监控', 'druid/sql.html', null, '1', 'fa fa-bug', '4');
INSERT INTO `sys_menu` VALUES ('15', '11', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '11', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '11', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '11', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '12', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '12', '新增', null, 'sys:role:save,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '12', '修改', null, 'sys:role:update,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '12', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '13', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '13', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '13', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '13', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'modules/sys/config.html', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'fa fa-sun-o', '6');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'modules/sys/log.html', 'sys:log:list', '1', 'fa fa-file-text-o', '7');
INSERT INTO `sys_menu` VALUES ('31', '1', '部门管理', 'modules/sys/dept.html', null, '1', 'fa fa-file-code-o', '1');
INSERT INTO `sys_menu` VALUES ('32', '31', '查看', null, 'sys:dept:list,sys:dept:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('33', '31', '新增', null, 'sys:dept:save,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('34', '31', '修改', null, 'sys:dept:update,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('35', '31', '删除', null, 'sys:dept:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('36', '1', '字典管理', 'modules/sys/dict.html', null, '1', 'fa fa-bookmark-o', '6');
INSERT INTO `sys_menu` VALUES ('37', '36', '查看', null, 'sys:dict:list,sys:dict:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('38', '36', '新增', null, 'sys:dict:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('39', '36', '修改', null, 'sys:dict:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('40', '36', '删除', null, 'sys:dict:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('41', '2', '用户管理', 'modules/manage/user.html', null, '1', 'fa fa-user', '6');
INSERT INTO `sys_menu` VALUES ('42', '41', '查看', null, 'manage:user:list,manage:user:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('43', '41', '新增', null, 'manage:user:save,manage:user:select', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('44', '41', '修改', null, 'manage:user:update,manage:user:select', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('45', '41', '删除', null, 'manage:user:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('46', '41', '充值', null, 'manage:user:topup', '2', null, '7');
INSERT INTO `sys_menu` VALUES ('47', '41', '提现', null, 'manage:user:cash', '2', null, '7');
INSERT INTO `sys_menu` VALUES ('48', '2', '充值提现记录', 'modules/manage/topupcashlog.html', null, '1', 'fa fa-money', '5');
INSERT INTO `sys_menu` VALUES ('49', '48', '查看', null, 'manage:topupcashlog:list,manage:topupcashlog:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('50', '48', '新增', null, 'manage:topupcashlog:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('51', '48', '修改', null, 'manage:topupcashlog:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('52', '48', '删除', null, 'manage:topupcashlog:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('53', '2', '基本参数设置', 'modules/manage/basesetting.html', null, '1', 'fa fa-toggle-on', '1');
INSERT INTO `sys_menu` VALUES ('54', '53', '查看', null, 'manage:basesetting:list,manage:basesetting:info', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('55', '53', '新增', null, 'manage:basesetting:save', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('56', '53', '修改', null, 'manage:basesetting:update', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('57', '53', '删除', null, 'manage:basesetting:delete', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('58', '2', '聊天室设置', 'modules/manage/group.html', null, '1', 'fa fa-home', '2');
INSERT INTO `sys_menu` VALUES ('59', '58', '查看', null, 'manage:group:list,manage:group:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('60', '58', '新增', null, 'manage:group:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('61', '58', '修改', null, 'manage:group:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('62', '58', '删除', null, 'manage:group:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('63', '2', '聊天室角色设置', 'modules/manage/usergroup.html', null, '1', 'fa fa-graduation-cap', '2');
INSERT INTO `sys_menu` VALUES ('64', '63', '查看', null, 'manage:usergroup:list,manage:usergroup:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('65', '63', '新增', null, 'manage:usergroup:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('66', '63', '修改', null, 'manage:usergroup:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('67', '63', '删除', null, 'manage:usergroup:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('68', '36', 'listByType', null, 'sys:dict:typelist', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('69', '2', '公告管理', 'modules/manage/announcement.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('70', '69', '查看', null, 'manage:announcement:list,manage:announcement:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('71', '69', '新增', null, 'manage:announcement:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('72', '69', '修改', null, 'manage:announcement:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('73', '69', '删除', null, 'manage:announcement:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('94', '2', '广播管理功能', 'modules/manage/broadcast.html', '', '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('95', '94', '查看', null, 'manage:broadcast:list,manage:broadcast:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('96', '94', '新增', null, 'manage:broadcast:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('97', '94', '修改', null, 'manage:broadcast:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('98', '94', '删除', null, 'manage:broadcast:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('99', '2', '应用管理功能', 'modules/manage/appoperating.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('100', '99', '查看', null, 'manage:appoperating:list,manage:appoperating:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('101', '99', '新增', null, 'manage:appoperating:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('102', '99', '修改', null, 'manage:appoperating:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('103', '99', '删除', null, 'manage:appoperating:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('104', '2', '登陆用户IP地址', 'modules/manage/ipaddress.html', null, '1', 'fa fa-file-code-o', '6');
INSERT INTO `sys_menu` VALUES ('105', '104', '查看', null, 'manage:ipaddress:list,manage:ipaddress:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('106', '104', '新增', null, 'manage:ipaddress:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('107', '104', '修改', null, 'manage:ipaddress:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('108', '104', '删除', null, 'manage:ipaddress:delete', '2', null, '6');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '超级管理员', '1', '2019-10-07 19:59:59');
INSERT INTO `sys_role` VALUES ('2', '管理员', '管理员', '3', '2019-10-07 20:00:29');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色与部门对应关系';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@lbadmin.9527', '13612345678', '1', '1', '2019-10-11 11:11:11');
INSERT INTO `sys_user` VALUES ('2', 'lbadmin', '3c98b62cde14c68f3e8a9a3bf7cc61e17662221b10ba238f73eb275903681727', '5HOkdfBkfUkKvaf69PRz', '123456@gmail.com', '13812345678', '1', '4', '2019-10-05 15:26:51');
INSERT INTO `sys_user` VALUES ('3', 'qw4530847', 'f1a64d35b4136275088eddead11f78a76d4db9602cd6fd3bcec8153442fa76cf', 'wkZy1LoL4QOG8lYLvbY5', '2176169440@qq.com', '13205556363', '1', '4', '2019-10-14 20:33:18');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1', '2');
INSERT INTO `sys_user_role` VALUES ('3', '3', '1');
INSERT INTO `sys_user_role` VALUES ('5', '4', '2');
INSERT INTO `sys_user_role` VALUES ('6', '4', '1');
INSERT INTO `sys_user_role` VALUES ('7', '5', '1');
INSERT INTO `sys_user_role` VALUES ('8', '6', '1');
INSERT INTO `sys_user_role` VALUES ('9', '6', '2');
